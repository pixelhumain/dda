/* uiCoop is use for all function relative to UI for Cooperation Spaces (DDA) 

$("#modalCoop").modal('show')
uiCoop.startUI();

*/
var uiCoop = {
	moduleIdCoop : "dda",
	inDDAInterface : false,
	startUI : function(loadData=true){
		mylog.log("startUICOOP");
		//$("#menu-left-container").hide();
		//$("#div-reopen-menu-left-container").removeClass("hidden");
		$("#main-coop-container").html("");
		$("#modal-preview-coop").html("");
		$("#modal-preview-coop").hide(300);
		uiCoop.inDDAInterface=true;
		$("#btn-close-coop").click(function(){
			onchangeClick=false;
			location.hash=hashUrlPage;
			uiCoop.inDDAInterface=false;
			//loadNewsStream(true);
			//if(contextData.slug != "undefined")
			//	location.hash="#@"+contextData.slug;
			$("#coop-data-container").html("");
		});
		//coInterface.scrollTo("#div-reopen-menu-left-container");

		//toogleNotif(false);

		$("a.title-section").off().click(function(){
			if($(this).hasClass("open")){
				$("#menuCoop .sub-"+$(this).data("key")).addClass("hidden");
				$(this).removeClass("open");
				$(this).find(".fa-caret-down").removeClass("fa-caret-down").addClass("fa-caret-right");
			}else{
				$("#menuCoop .sub-"+$(this).data("key")).removeClass("hidden");
				$(this).addClass("open");
				$(this).find(".fa-caret-right").removeClass("fa-caret-right").addClass("fa-caret-down");
			}
		});

		uiCoop.initBtnLoadData();

		uiCoop.getCoopData(contextData.type, contextData.id, "menucoop");
		
		if(loadData){
			uiCoop.getCoopData(contextData.type, contextData.id, "proposal");
		}
	},

	closeUI : function(reloadStream){
		if(location.hash.indexOf("#dda") >= 0){
			uiCoop.inDDAInterface=false;
			onchangeClick=false;
			location.hash="#dda";
		}
	},

	initBtnLoadData : function(){
		//alert('initBtnLoadData');
		$(".load-coop-data").off().click(function(){
			//alert('initBtnLoadData');
			mylog.log("load-coop-data");
			var type = $(this).data("type");
			
			if(type == "locked"){
				toastr.info("Vous n'avez pas accès à cet espace");
				return;
			}

			$("#menu-room .load-coop-data").removeClass("active");
			$(".load-coop-data[data-type='"+type+"']").removeClass("active");

			$(this).addClass("active");

			var type = $(this).data("type");
			var status = $(this).data("status");
			var dataId = $(this).data("dataid");
			var menu = $(this).data("menu");
			
			//mylog.log("LOAD COOP DATA", contextData.type, contextData.id, type, status, dataId);
			pid = (contextData && contextData.id) ? contextData.id : userId;
	    	ptype = (contextData && contextData.type) ? contextData.type : "citoyens";
	    	//alert("initBtnLoadData "+type)
			if(notNull(menu))
				uiCoop.getCoopData(ptype, pid, type, status, dataId);
			else 
				uiCoop.getCoopDataPreview(type, dataId, status);
			
		});
	},

	initBtnLoadDataPreview : function(){
		//alert('initBtnLoadDataPreview');
		$(".load-coop-data").off().click(function(){
			//alert('initBtnLoadDataPreview');
			var type = $(this).data("type");
			if(type == "locked"){
				toastr.info("Vous n'avez pas accès à cet espace");
				return;
			}

			//mylog.log("LOAD COOP DATA", contextData.type, contextData.id, type, status, dataId);
			var type = $(this).data("type");
			var status = $(this).data("status");
			var dataId = $(this).data("dataid");
			var menu = $(this).data("menu");
			//mylog.log("LOAD COOP DATA", contextData.type, contextData.id, type, status, dataId);
			pid = (contextData && contextData.id) ? contextData.id : userId;
	    	ptype = (contextData && contextData.type) ? contextData.type : "citoyens";
	    	//alert("initBtnLoadDataPreview : "+type+preview);
			if(notNull(menu))
				uiCoop.getCoopData(ptype, pid, type, status, dataId);
			else 
				uiCoop.getCoopDataPreview(type, dataId, status);
				
			
		});
	},

	initDragAndDrop : function(){ mylog.log('initDragAndDrop');
		$('.draggable').draggable({
		    revert : true, // sera renvoyé à sa place s'il n'est pas déposé dans #drop
		    appendTo: 'body',
		    helper: 'clone',
		    zIndex: 10000,
    		scroll: false,
    		start : function(){
    			uiCoop.dragId = $(this).data("dataid");
    			uiCoop.dragType = $(this).data("type");
    			mylog.log("start drag", $(this).data("dataid"), "coopId", uiCoop.dragId, "coopType", uiCoop.dragType);
    		}
		});
		$('.droppable').droppable({
			accept : '.draggable', // je n'accepte que le bloc ayant "draggable" pour class
		    drop : function(event, ui){
		        var idNewRoom = $(this).data("dataid");
		        uiCoop.changeRoom(uiCoop.dragType, uiCoop.dragId, idNewRoom, contextData.type, contextData.id);
		    	toastr.info("L'élément a bien été déplacé");
		    },
		    activate : function( event, ui ){
		    	var roomid = $(this).data("dataid");
		    	//mylog.log("activate", roomid);
		    	$(this).parent().addClass("bg-lightblue draggin");
		    },
		    deactivate : function( event, ui ){
		    	var roomid = $(this).data("dataid");
		    	//mylog.log("deactivate", roomid);
		    	$(this).removeClass("text-white").parent().removeClass("bg-lightblue draggin bg-turq");
		    },
		    over : function( event, ui ){
		    	var roomid = $(this).data("dataid");
		    	mylog.log("over", roomid);
		    	$(this).addClass("text-white").parent().addClass("bg-turq");
		    	$(this).parent().removeClass("bg-lightblue");
		    },
		    out : function( event, ui ){
		    	var roomid = $(this).data("dataid");
		    	mylog.log("out", roomid);
		    	$(this).removeClass("text-white").parent().removeClass("bg-turq");
		    	$(this).parent().addClass("bg-lightblue");
		    }
		});
	},

	minimizeMenuRoom : function(min){ mylog.log("minimizeMenuRoom", min);
		if(min)	{
			$("#menu-room").addClass("min col-sm-4 col-md-4 col-lg-4").removeClass("col-sm-12 col-md-12 col-lg-12");
			$(uiCoop.getParentContainer()+" #coop-data-container").addClass("col-sm-8").removeClass("hidden");
		}
		else{
			uiCoop.maximizeReader(false);
			$("#menu-room").removeClass("min col-sm-4 col-md-4 col-lg-4").addClass("col-sm-12 col-md-12 col-lg-12");
			$(uiCoop.getParentContainer()+" #coop-data-container").removeClass("min col-sm-8").addClass("hidden");
		}
	},

	maximizeReader : function(max){ mylog.log("maximizeReader", max);
		if(max)	{
			$("#menu-room").addClass("hidden");
			$(uiCoop.getParentContainer()+" #coop-data-container").removeClass("col-sm-8 col-md-8 col-lg-8").addClass("col-sm-12 col-md-12 col-lg-12");
		}
		else{
			$("#menu-room").removeClass("hidden");
			$(uiCoop.getParentContainer()+" #coop-data-container").removeClass("col-sm-12 col-md-12 col-lg-12").addClass("col-sm-8 col-md-8 col-lg-8");
		}
	},

	showAmendement : function(show){
		if(show){
			$("#menu-room").addClass("hidden");
			//$("#coop-data-container").addClass("col-sm-12").removeClass("col-sm-8");
			$("#amendement-container").removeClass("hidden");

		}else{
			$("#menu-room").removeClass("hidden");
			//$("#coop-data-container").removeClass("col-sm-12").addClass("col-sm-8");
			$("#amendement-container").addClass("hidden");
		
		}
	},

	getCoopData : function(parentType, parentId, type, status, dataId, onSuccess, showLoading){
		mylog.log("getCoopData", parentType, parentId, type, status, dataId, onSuccess, showLoading)
		var url = baseUrl+'/dda/co/getcoopdata';
		var params = {
			"parentType" : parentType,
			"parentId" : parentId,
			"type" : type,
			"status" : status,
			"dataId" : dataId,
			"json" : false
		};
		//mylog.log("showLoading ?", typeof showLoading, showLoading);
		
		if(typeof showLoading == "undefined" || showLoading == true){
			if(typeof dataId == "undefined" || dataId == null || type == "room"){
				coInterface.showLoader("#main-coop-container");
				//$("#main-coop-container").html(
				//	"<h2 class='margin-top-50 text-center'><i class='fa fa-refresh fa-spin'></i></h2>");
			}
			else{
				coInterface.showLoader(uiCoop.getParentContainer()+" #coop-data-container");
				//$(uiCoop.getParentContainer() + "#coop-data-container").html(
				//	"<h2 class='margin-top-50 text-center'><i class='fa fa-refresh fa-spin'></i></h2>");
			}
		}

		ajaxPost("", url, params,
			function (data){
				if(typeof dataId == "undefined" || dataId == null || type == "room") {
					if(type == "menucoop")
						$("#menuCoop").html(data);
					else if(dataId == null && type == "room")
						$("#coop-room-list").html(data);
					else
						$("#main-coop-container").html(data);
					uiCoop.minimizeMenuRoom(false);
				}
				else{ 
					$(uiCoop.getParentContainer()+" #coop-data-container").html(data);
					if(uiCoop.getParentContainer().indexOf("modal-preview-coop")<=0)
						uiCoop.minimizeMenuRoom(true);
				}

				uiCoop.initBtnLoadData();

				$(".btn-hide-data-room").off().click(function(){
					if($(this).hasClass("open")){
						$("#menu-room .sub-"+$(this).data("key")).addClass("hidden");
						$(this).removeClass("open");
						//$(this).find(".fa-caret-down").removeClass("fa-caret-down").addClass("fa-caret-right");
					}else{
						$("#menu-room .sub-"+$(this).data("key")).removeClass("hidden");
						$(this).addClass("open");
						//$(this).find(".fa-caret-right").removeClass("fa-caret-right").addClass("fa-caret-down");
					}
				});

				$(".tooltips").tooltip();

				if(typeof onSuccess == "function") onSuccess();
			}
		);
	},
	prepPreview : function(coopType,coopId,idParentRoom,parentId,parentType,afterLoad) { 

	    if( $(this).data("coop-section") ){
	        coopSection = $(this).data("coop-section");
	        if(coopSection == "amendments" ){
	            afterLoad = function() { 
	                uiCoop.showAmendement(true);
	                if($("#form-amendement").hasClass("hidden"))
	                    $("#form-amendement").removeClass("hidden");
	                else 
	                    $("#form-amendement").addClass("hidden");
	            };
	        } else if(coopSection == "vote" ){
	            afterLoad = function() { 
	                setTimeout(function(){
	                  // $("#coop-container").animate({
	                  //       scrollTop: $("#podVote").offset().top
	                  //   }, 1000);
	                }, 1000);
	            };
	        }
	        else if(coopSection == "comments" ){
	            afterLoad = function() { 
	                setTimeout(function(){
	                  $("#coop-container").animate({
	                        scrollTop: $(".btn-select-arg-comment").offset().top
	                    }, 1000);
	                }, 1000);
	                
	            };
	        }
	    }
	    coopType = coopType == "actions" ? "action" : coopType;
	    coopType = coopType == "proposals" ? "proposal" : coopType;
	    coopType = coopType == "resolutions" ? "resolution" : coopType;

	    

	    if(notNull(contextData) && contextData.id == parentId && contextData.type == parentType && typeof isOnepage == "undefined" && idParentRoom != ""){
	        toastr.info(trad["processing"]);
	        uiCoop.startUI();
	        $("#modalCoop").modal("show");
	        onchangeClick=false;
	        if(coopType == "rooms"){
	          uiCoop.getCoopData(contextData.type, contextData.id, "room", null, coopId);
	        }else{
	            setTimeout(function(){
	              uiCoop.getCoopData(contextData.type, contextData.id, "room", null, idParentRoom, 
	              function(){
	                toastr.info(trad["processing"]);
	                uiCoop.getCoopData(contextData.type, contextData.id, coopType, null, coopId);
	              }, false);
	            }, 1000);
	        }
	    }else{
	        if(coopType == "rooms"){
	          var hash = "#page.type." + parentType + ".id." + parentId + 
	                ".view.coop.room." + idParentRoom + "."+coopType+"." + coopId;
	          urlCtrl.loadByHash(hash);
	        }else{
	          uiCoop.getCoopDataPreview(coopType, coopId, afterLoad);
	        }
	    }
	},
	getCoopDataPreview : function(type, dataId, onSuccess, showLoading){
		mylog.log("HERE getCoopDatagetCoopDataPreview", $("#modal-preview-coop").length, type, status, dataId, onSuccess, showLoading);
		
		setTimeout(function(){
			mylog.log("HERE setTimeout", $("#modal-preview-coop").length);
			$("#modal-preview-coop").removeClass("hidden")
									.css("display","block");
			coInterface.showLoader("#modal-preview-coop");
									//.html("<i class='fa faspin fa-circle-o-notch padding-25 fa-2x letter-turq'></i>");
		
			var url = baseUrl+'/dda/co/previewcoopdata';
			var params = {
				"type" : type,
				"dataId" : dataId,
				"json" : false
			};
			ajaxPost("", url, params,
				function (data){
					mylog.log("HERE ajaxPost start", $("#modal-preview-coop").length);
					$("#modal-preview-coop").html(data);

					$("#btn-refresh-"+type).off().click(function(){
						toastr.info(trad["processing"]);
						var idData = $(this).data("id-"+type);
						uiCoop.getCoopDataPreview(type, idData);
					});
					//uiCoop.initBtnLoadData();
					//alert();
					//$(".addAmendTxt").prepend("Rappel : les mesures et amendements doivent s'inscrire dans les champs de compétences des communes et intercommunalités pour qu'elles puissent être mises en oeuvre.<br/>");

					$(".tooltips").tooltip();
					$("#modal-preview-coop").css("display","block");
					coInterface.bindLBHLinks();
					urlCtrl.bindModalPreview();
					if(typeof onSuccess == "function") onSuccess();
					//mylog.log("HERE ajaxPost end",$("#modal-preview-coop").length, data );
				},
				function(data){
					$("#modal-preview-coop").html("error");
				}
			);
		}, 200);
								
		
	},

	initSearchInMenuRoom : function(){
		$(".inputSearchInMenuRoom").keyup(function(){
			var type = $(this).data('type-search');
			var searchVal = $(this).val();
			if(searchVal == "") {
				$(".submenucoop.sub-"+type).show();
				return;
			}

			$("#coop-container .submenucoop.sub-"+type).hide();
			mylog.log("searchVal", searchVal, "type", type);
			$.each($("#coop-container .submenucoop.sub-"+type), function(){
				mylog.log("this", this);
				
				var content = $(this).data("name-search");

				if(typeof content != "undefined"){
					var found = content.search(new RegExp(searchVal, "i"));
					mylog.log("content", content);
					if(found >= 0){
						var id = $(this).show();
					}
				}
			});
			
		});
	},

	sendVote : function(parentType, parentId, voteValue, idParentRoom, idAmdt, returnJson=false){
		

		var params = {
			"parentType" : parentType,
			"parentId" : parentId,
			"voteValue" : voteValue,
			"json" : returnJson
		};
		if(typeof idAmdt != "undefined")
			params["idAmdt"] = idAmdt;

		var url = baseUrl+"/dda/co/savevote";
		
		toastr.info(trad["processing save"]);
		ajaxPost("", url, params,
			function (proposalRes){
				
				pid = (contextData && contextData.id) ? contextData.id : userId;
	    		ptype = (contextData && contextData.type) ? contextData.type : "citoyens";
				uiCoop.getCoopData(ptype, pid, "room", null, idParentRoom, 
					function(){
						toastr.success(trad["Your vote has been save with success"]);
						
						if(returnJson == false){
							if(uiCoop.getParentContainer().indexOf("modal-preview-coop")<=0)
								uiCoop.minimizeMenuRoom(true);
							else{
								uiCoop.minimizeMenuRoom(true);
								uiCoop.maximizeReader(true);
							}
							//else
							//	uiCoop.minimizeMenuRoom();
							$(uiCoop.getParentContainer()+"#coop-data-container").html(proposalRes);
							if(parentType == "amendement"){
								uiCoop.showAmendement(true);
							}
						}else{
							
							var blockId = ".newsActivityStream"+parentId;
							if(location.hash.indexOf('#dda') == 0)
								blockId = ".blockCoop"+parentId;
							
							$(blockId).html(directory.coopPanelHtml(proposalRes["proposal"]));
							directory.bindBtnElement();
						}	
					}, false);
			}
		);
	},

	activateVote : function(proposalId){
		
		var param = {
			block: "activeCoop",
			typeElement: "proposals",
			id: proposalId,
			
			status: "tovote",
			voteActivated: true,
			amendementActivated: false

		};
		ajaxPost(
		    null,
	        baseUrl+"/"+moduleId+"/element/updateblock/",
	        param,
	        function(data){ 
	        	uiCoop.getCoopData(contextData.type, contextData.id, "proposal", null, proposalId);
	        }
        );
	},

	saveAmendement : function(proposalId, typeAmdt){
		var txtAmdt = $("#txtAmdt").val();
		if(txtAmdt.length < 10){
			toastr.error(trad.amendementTooShort);
			return;
		}

		var param = {
			block: "amendement",
			typeElement: "proposals",
			id: proposalId,			
			txtAmdt: txtAmdt,
			typeAmdt: typeAmdt
		};
		mylog.log("saveAmendement", param);
		toastr.info(trad["processing save"]);

		ajaxPost(
		    null,
	        baseUrl+"/"+moduleId+"/element/updateblock/",
	        param,
	        function(data){ 
	        	mylog.log("success updateblock", data);
				pid = (contextData && contextData.id) ? contextData.id : userId;
    			ptype = (contextData && contextData.type) ? contextData.type : "citoyens";
	    		uiCoop.getCoopData(ptype, pid, "proposal", null, proposalId, function(){
	    			uiCoop.showAmendement(true);
	    			toastr.success(trad["Your amendement has been save with success"]);
	    		}, false);
	        }
        );

	},

	changeStatus : function(type, id, status, parentType, parentId){
		var param = {
			parentType : parentType,
			parentId : parentId,
			type: type,
			id: id,			
			name: "status",
			value: status
		};
		
		toastr.info(trad["processing save"]);
		ajaxPost(
		    null,
	        baseUrl+"/"+moduleId+"/element/updatefield/",
	        param,
	        function(data){ 
	        	type = (type == "proposals") ? "proposal" : type;
	    		type = (type == "actions") ? "action" : type;
	    		toastr.success(trad["processing ok"]);
	    		pid = (contextData && contextData.id) ? contextData.id : userId;
    			ptype = (contextData && contextData.type) ? contextData.type : "citoyens";
				uiCoop.getCoopData(ptype, pid, "room", null, idParentRoom, function(){
		    		uiCoop.getCoopData(parentType, parentId, type, null, id);
	    		});
	        }
        );
	},

	changeRoom : function(dragType, dragId, idNewRoom, parentType, parentId){
		var param = {
			parentType : parentType,
			parentId : parentId,
			type: dragType,
			id: dragId,			
			name: "idParentRoom",
			value: idNewRoom
		};
		mylog.log("changeRoom", param);
		toastr.info(trad["processing save"]);
		ajaxPost(
		    null,
	        baseUrl+"/"+moduleId+"/element/updatefield/",
	        param,
	        function(data){ 
	        	uiCoop.getCoopData(contextData.type, contextData.id, "room", null, idNewRoom);
	        }
        );
	},

	deleteByTypeAndId : function(typeDelete, idDelete){
		bootbox.dialog({
                message: '<div class="row">  ' +
                    '<div class="col-md-12"> ' +
                    '<span>'+trad.areyousuretodelete+' ?</span> ' +
                    '</div></div>',
                buttons: {
                    success: {
                        label: "Ok",
                        className: "btn-primary",
                        callback: function () {
                            toastr.info(trad["processing save"]);
                            ajaxPost(
							    null,
						        baseUrl+"/co2/element/delete/type/"+typeDelete+"/id/"+idDelete,
						        {},
						        function(data){ 
						        	urlCtrl.loadByHash(location.hash);
						        }
					        );	  
                        }
                    },
                    cancel: {
                    	label: trad["cancel"],
                    	className: "btn-secondary",
                    	callback: function() {}
                    }
                }
            }); 

	},

	deleteAmendement : function(numAm, idProposal){
		var param = {
			numAm : numAm,
			idProposal : idProposal,
			json : false
		};
		
		toastr.info(trad["processing delete"]);
		ajaxPost(
		    null,
	        baseUrl+"/dda/co/deleteamendement/",
	        {},
	        function(data){ 
	        	toastr.success(trad["processing delete ok"]);
				$("#amendementBlock"+numAm).remove();
	        },
	        function(data){
	    		mylog.log("error data ", data);
	    		toastr.error(trad["processing delete KO"]);
	    	}
        );
	},

	assignMe : function(idAction){
		ajaxPost(
		    null,
	        baseUrl+"/"+moduleId+"/rooms/assignme",
	        { "id" : idAction },
	        function(data){ 
	        	if(data.result){
	    		  toastr.success(trad.thxForParticipation);
	              urlCtrl.loadByHash(location.hash);
                }
                else 
                  toastr.error(data.msg);
	        }
        );	
	},

	getParentContainer : function(){
		var res = ($("#modal-preview-coop #coop-container").length >= 0) ? "#modalCoop " : "#modal-preview-coop ";
		mylog.log("getParentContainer", res);
		return ($("#modalCoop #coop-container").length > 0) ? "#modalCoop " : "#modal-preview-coop ";
	},
	getUrlPreviewInDda : function(hash){

	},
	initUIProposal : function(){
		
		$("#comments-container").html("<i class='fa fa-spin fa-refresh'></i> " + trad.loadingComments);
		
		$(".footer-comments").html("");
		getAjax("#comments-container",baseUrl+"/"+moduleId+"/comment/index/type/proposals/id/"+idParentProposal,
			function(){  //$(".commentCount").html( $(".nbComments").html() ); 
				$(".container-txtarea").hide();

				$(".btn-select-arg-comment").click(function(){
					var argval = $(this).data("argval");
					$(".container-txtarea").show();

					$(".textarea-new-comment").removeClass("bg-green-comment bg-white-comment bg-red-comment");
					var classe="";
					var pholder="Votre commentaire";
					if(argval == "up")   { classe="bg-green-comment"; pholder="Votre argument pour";   }
					if(argval == "down") { classe="bg-red-comment";   pholder="Votre argument contre"; }
					$(".textarea-new-comment").addClass(classe).attr("placeholder", pholder);
					$("#argval").val(argval);
				});

		},"html");

		$("#btn-close-proposal").click(function(){
			uiCoop.minimizeMenuRoom(false);
		});

		$("#modal-preview-coop #btn-close-proposal").off().click(function(){
			urlCtrl.closePreview();
		});

		$(".btn-extend-proposal").click(function(){
			uiCoop.maximizeReader(true);
			$(".btn-minimize-proposal").removeClass("hidden");
			$(".btn-extend-proposal").addClass("hidden");
		});

		$(".btn-minimize-proposal").click(function(){
			uiCoop.maximizeReader(false);
			$(".btn-minimize-proposal").addClass("hidden");
			$(".btn-extend-proposal").removeClass("hidden");
		});

		$(".btn-show-amendement").click(function(){
			uiCoop.showAmendement(true);
		});

		$("#btn-hide-amendement").click(function(){
			uiCoop.showAmendement(false);
		});

		$(".btn-create-amendement").click(function(){
			uiCoop.showAmendement(true);
			if($("#form-amendement").hasClass("hidden"))
				$("#form-amendement").removeClass("hidden");
			else 
				$("#form-amendement").addClass("hidden");
		});

		$(".btn-modal-delete-am").click(function(){ //alert(".btn-modal-delete-am " + $(this).data("id-am"));
			var idAm = $(this).data("id-am");
			//$("#btn-delete-am").attr("data-id-am", idAm);


			$("#btn-delete-am").off().click(function(){
				//var idAm = $(this).data("id-am");
				uiCoop.deleteAmendement(idAm, idParentProposal);
			});
		});


		$("#podVote .btn-send-vote").off().click(function(){
			var voteValue = $(this).data('vote-value');
			mylog.log("send vote", voteValue, idParentProposal);
			uiCoop.sendVote("proposal", idParentProposal, voteValue, idParentRoom);
		});

		$("#btn-activate-vote").click(function(){
			uiCoop.activateVote(idParentProposal);
		});

		$("#btn-refresh-proposal").click(function(){
			toastr.info(trad["processing"]);
			var idProposal = $(this).data("id-proposal");
			pid = (contextData && contextData.id) ? contextData.id : userId;
	    	ptype = (contextData && contextData.type) ? contextData.type : "citoyens";
		    		
			uiCoop.getCoopData(ptype, pid, "proposal", null, idProposal, 
				function(){
					uiCoop.minimizeMenuRoom(true);
					uiCoop.showAmendement(false);
					toastr.success(trad["processing ok"]);
				}, false);
		});

		$("#btn-refresh-amendement").click(function(){
			toastr.info(trad["processing"]);
			var idProposal = $(this).data("id-proposal");
			pid = (contextData && contextData.id) ? contextData.id : userId;
	    	ptype = (contextData && contextData.type) ? contextData.type : "citoyens";		
			uiCoop.getCoopData(ptype, pid, "proposal", null, idProposal, 
				function(){
					uiCoop.minimizeMenuRoom(true);
					uiCoop.showAmendement(true);
					toastr.success(trad["processing ok"]);
				}, false);
		});

		$(".btn-option-status-proposal").click(function(){
			var idProposal = $(this).data("id-proposal");
			var status = $(this).data("status");
			mylog.log("update status proposals", idProposal, status, parentTypeElement, parentIdElement);
			uiCoop.changeStatus("proposals", idProposal, status, parentTypeElement, parentIdElement);
		});

		$(".btn-option-delete-proposal").click(function(){
			var idProposal = $(this).data("id-proposal");
			var status = $(this).data("status");
			mylog.log("delete status proposals", idProposal, status, parentTypeElement, parentIdElement);
			uiCoop.deleteByTypeAndId("proposals", idProposal);
			$("#modal-preview-coop").hide(300);
		});
		$(".btn-option-delete-proposal-custom-admin").click(function(){
			var params={
				id: $(this).data("id-proposal"),
				type:"proposals"
			};
			if(typeof costum != "undefined" && notNull(costum) && costum.slug){
				params.origin="costum";
				params.sourceKey=costum.slug;
			} 
			ajaxPost(
			    null,
		        baseUrl+"/"+moduleId+"/admin/setsource/action/remove/set/source",
		        params,
		        function(data){ 
		        	 if ( data && data.result ) {
			        	urlCtrl.loadByHash(location.hash);
			        	$("#modal-preview-coop").hide(300);
			        } else {
			           toastr.error("something went wrong!! please try again.");
			        }
		        }
	        );
		});
		$("#btn-edit-proposal").click(function(){
			var idProposal = $(this).data("id-proposal");
			mylog.log("edit idProposal", idProposal);
			dyFObj.editElement('proposals', idProposal);
		});

		$(".descriptionMarkdown").html(dataHelper.convertMardownToHtml($(".descriptionMarkdown").html()));
		
		$("#container-text-proposal").html(dataHelper.markdownToHtml($("#container-text-proposal").html()) );
		$("#container-text-complem").html(dataHelper.markdownToHtml($("#container-text-complem").html()) );

		if($("#modal-preview-coop #coop-container").length == 0){
			var addCoopHash=".view.coop.room." + idParentRoom + ".proposal." + idParentProposal;
			onchangeClick=false;
			if(typeof hashUrlPage != "undefined")
				location.hash = hashUrlPage +addCoopHash;
			else if(notNull(contextData) && typeof contextData.slug != "undefined")
				location.hash = "#@" + contextData.slug + addCoopHash;
			else
				location.hash = "#page.type." + parentTypeElement + ".id." + parentIdElement +addCoopHash; 
		}

		//uiCoop.initBtnLoadDataPreview();

		if(msgController != ""){
			toastr.error(msgController);
		}
	},

	initUIAction : function(){

		$("#comments-container").html("<i class='fa fa-spin fa-refresh'></i> " + trad.loadingComments);
		
		$(".footer-comments").html("");

		getAjax("#comments-container",baseUrl+"/"+moduleId+"/comment/index/type/actions/id/"+idAction,
			function(){  //$(".commentCount").html( $(".nbComments").html() ); 
		},"html");


		$("#btn-close-action").click(function(){
			uiCoop.minimizeMenuRoom(false);
		});
		$("#modal-preview-coop #btn-close-action").off().click(function(){
			$("#modal-preview-coop").hide(300);
		});

		$(".btn-extend-action").click(function(){
			uiCoop.maximizeReader(true);
			$(".btn-minimize-action").removeClass("hidden");
			$(".btn-extend-action").addClass("hidden");
		});

		$(".btn-minimize-action").click(function(){
			uiCoop.maximizeReader(false);
			$(".btn-minimize-action").addClass("hidden");
			$(".btn-extend-action").removeClass("hidden");
		});

		$("#btn-refresh-action").click(function(){
			toastr.info(trad["processing"]);
			var idProposal = $(this).data("id-action");
			
			uiCoop.getCoopData(contextData.type, contextData.id, "action", null, idProposal, 
				function(){
					uiCoop.minimizeMenuRoom(true);
					uiCoop.showAmendement(false);
					toastr.success(trad["processing ok"]);
				}, false);
		});

		$(".btn-option-status-action").click(function(){
			var idAction = $(this).data("id-action");
			var status = $(this).data("status");
			mylog.log("update status actions", idAction, status, parentTypeElement, parentIdElement);
			uiCoop.changeStatus("actions", idAction, status, parentTypeElement, parentIdElement);
		});

		$("#btn-edit-action").click(function(){
			var idaction = $(this).data("id-action");
			mylog.log("edit idAction", idAction);
			dyFObj.editElement('actions', idAction);
		});

		$("#btn-validate-assign-me").off().click(function(){
			uiCoop.assignMe(idAction);
		});

		$(".btn-assignee").off().click(function(){
			
			setTimeout(function(){	
				$('#modalAssignMe').modal("show");
				$('#modalAssignMe').css("z-index","100000");
			},800);
		});

		$(".btn-assign-someone").off().click(function(){
			
			setTimeout(function(){	
				$('#modalLinkAction').modal("show");
				$('#modalLinkAction').css("z-index","100000");
			},800)
		});

		if($("#modal-preview-coop #coop-container").length == 0){
			var addCoopHash=".view.coop.room." + idParentRoom + ".action." + idAction;
			onchangeClick=false;
			if(typeof hashUrlPage != "undefined")
				location.hash = hashUrlPage +addCoopHash;
			else if(notNull(contextData) && typeof contextData.slug != "undefined")
				location.hash = "#@" + contextData.slug + addCoopHash;
			else
				location.hash = "#page.type." + parentTypeElement + ".id." + parentIdElement +addCoopHash;  
		}					  
	},

	initUIResolution : function(){
		$("#comments-container").html("<i class='fa fa-spin fa-refresh'></i> Chargement des commentaires");
		
		getAjax("#comments-container",baseUrl+"/"+moduleId+"/comment/index/type/resolutions/id/"+idParentResolution,
			function(){  //$(".commentCount").html( $(".nbComments").html() ); 
				$(".container-txtarea").hide();

				$(".btn-select-arg-comment").click(function(){
					var argval = $(this).data("argval");
					$(".container-txtarea").show();

					$(".textarea-new-comment").removeClass("bg-green-comment bg-red-comment");
					var classe="";
					var pholder="Votre commentaire";
					if(argval == "up")   { classe="bg-green-comment"; pholder="Votre argument pour";   }
					if(argval == "down") { classe="bg-red-comment";   pholder="Votre argument contre"; }
					$(".textarea-new-comment").addClass(classe).attr("placeholder", pholder);
					$("#argval").val(argval);
				});
		},"html");

		$("#btn-close-resolution").click(function(){
			uiCoop.minimizeMenuRoom(false);
		});
		$("#modal-preview-coop #btn-close-resolution").off().click(function(){
			$("#modal-preview-coop").hide(300);
		});

		$(".btn-extend-resolution").click(function(){
			uiCoop.maximizeReader(true);
			$(".btn-minimize-resolution").removeClass("hidden");
			$(".btn-extend-resolution").addClass("hidden");
		});
		$(".btn-minimize-resolution").click(function(){
			uiCoop.maximizeReader(false);
			$(".btn-minimize-resolution").addClass("hidden");
			$(".btn-extend-resolution").removeClass("hidden");
		});
		$(".btn-show-amendement").click(function(){
			uiCoop.showAmendement(true);
		});
		$("#btn-hide-amendement").click(function(){
			uiCoop.showAmendement(false);
		});
		$(".btn-create-amendement").click(function(){
			uiCoop.showAmendement(true);
			if($("#form-amendement").hasClass("hidden"))
				$("#form-amendement").removeClass("hidden");
			else 
				$("#form-amendement").addClass("hidden");
		});

		$("#btn-activate-vote").click(function(){
			uiCoop.activateVote(idParentResolution);
		});

		$("#btn-refresh-resolution").click(function(){
			toastr.info(trad["processing"]);
			var idresolution = $(this).data("id-resolution");
			uiCoop.getCoopData(parentTypeElement, parentIdElement, "resolution", null, idresolution, 
				function(){
					uiCoop.minimizeMenuRoom(true);
					uiCoop.showAmendement(false);
					toastr.success(trad["processing ok"]);
				}, false);
		});

		$("#btn-refresh-amendement").click(function(){
			toastr.info(trad["processing"]);
			var idresolution = $(this).data("id-resolution");
			uiCoop.getCoopData(null, null, "resolution", null, idresolution, 
				function(){
					uiCoop.minimizeMenuRoom(true);
					uiCoop.showAmendement(true);
					toastr.success(trad["processing ok"]);
				}, false);
		});

		$(".btn-option-status-resolution").click(function(){
			var idresolution = $(this).data("id-resolution");
			var status = $(this).data("status");
			uiCoop.changeStatus("resolutions", idresolution, status, parentTypeElement, parentIdElement);
		});

		$("#btn-show-voteres").click(function(){
			if($(".podvote").hasClass("hidden")) $(".podvote").removeClass("hidden");
			else $(".podvote").addClass("hidden");
		});

		$("#btn-create-action").click(function(){
			useIdParentResolution = true;
			dyFObj.openForm('action');
		});

		if($("#modal-preview-coop #coop-container").length == 0){
			var addCoopHash=".view.coop.room." + idParentRoom + ".resolution." + idParentResolution;
			onchangeClick=false;
			if(typeof hashUrlPage != "undefined")
				location.hash = hashUrlPage +addCoopHash;
			else if(notNull(contextData) && typeof contextData.slug != "undefined")
				location.hash = "#@" + contextData.slug + addCoopHash;
			else
				location.hash = "#page.type." + parentTypeElement + ".id." + parentIdElement +addCoopHash;
		}


		$("#container-text-resolution").html(dataHelper.markdownToHtml($("#container-text-resolution").html()) );
		$("#container-text-complem").html(dataHelper.markdownToHtml($("#container-text-complem").html()) );

		if(msgController != ""){
			toastr.error(msgController);
		}
	},

	loadCoop : function(roomId, proposalId, resolutionId, actionId){
		/*
		if(userId == "") {
			toastr.info("Vous devez êtres connecté pour accéder à cet espace coopératif");
			loadNewsStream();
			return;
		}*/
		roomId 		= (roomId != "") 	 ? roomId 		: null;
		proposalId  = (proposalId != "") ? proposalId 	: null;
		resolutionId  = (resolutionId != "") ? resolutionId 	: null;
		actionId 	= (actionId != "") 	 ? actionId 	: null;

		toastr.info(trad["processing"]);
		
		uiCoop.startUI(false);
		
		setTimeout(function(){	
			uiCoop.getCoopData(contextData.type, contextData.id, "room", null, roomId, function(){ 
				//toastr.success(trad["processing ok"]);
				$("#modalCoop").modal("show");

				var type = null;
				var id = null;

				if(proposalId != null){
					type = "proposal"; 
					id = proposalId;
				}

				if(actionId != null){
					type = "action"; 
					id = actionId;
				}

				if(resolutionId != null){
					type = "resolution"; 
					id = resolutionId;
				}

				

				if(type != null) 
					uiCoop.getCoopData(contextData.type, contextData.id, type, null, id);

			});
		}, 1500);
	}


}