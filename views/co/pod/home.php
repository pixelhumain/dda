<style>
	.help-coop{
		padding: 0 15px;
	}

	/************  ESPACE CO   *****************/

	@media (min-width: 992px){
		.central-section .modal-lg {
		    width: 100%;
		}
	}
		
	@media (min-width: 768px){
		.central-section .modal-dialog {
		    width: 100%;
		    margin: auto;
		}
	}

	.central-section ul#menuCoop {
	    position: inherit;
	}
	.central-section #modalCoop .modal-content .modal-header{
		display: none;
	}	

	.font-montserrat .well{
		border-radius: 15px;
    	border: 3px solid #9fbd38;
    	background-color: #fff;
    	margin: 35px 0px;
    	text-align: center;
	}
	.font-montserrat .well .label-question{
		background-color: #9fbd38;
	    width: fit-content;
	    padding: 10px 20px;
	    border-radius: 20px;
	    text-align: center;
	    margin: auto;
	    margin-top: -45px;
	    color: #fff;
	}
	.font-montserrat .well .paragraph {
		padding-top: 15px;
    	font-size: 14px;
	}
	.font-montserrat .icon-before-label {
		border-radius: 50%;
	    width: 50px;
	    height: 50px;
	    color: #9fbd38;
	    text-align: center;
	    margin-right: 10px;
	    display: inline-block;
	    border: 2px solid #9fbd38;
	    background-color: #fff;
	    margin-left: -200px;
	    transition: all .3s ease-out;
	}
	.font-montserrat .icon-before-label:hover {
		transform: rotate(360deg);
	}

	.font-montserrat .label-start{
		font-size: 24px;
	    background-color: #9fbd38;
	    width: fit-content;
	    padding: 4px 25px;
	    border-radius: 20px;
	    margin-left: 40px;
	}

	.font-montserrat ul {
	    counter-reset: li; /* Initiate a counter */
	    list-style: none; /* Remove default numbering */
	    padding: 0;
	    text-shadow: 0 1px 0 rgba(255,255,255,.5);
  	}
  	.font-montserrat .rounded-list a{
	    position: relative;
	    display: block;
	    padding: 6.4px 6.4px 6.4px 32px;
	    *padding: .4em;
	    margin: 8px 0;
	    background: #9fbd38;
	    color: #3f4e58;
	    text-decoration: none;
	    border-radius: 4.8px;
	    transition: all .3s ease-out;
	    width: fit-content;
  	}

	.font-montserrat .rounded-list a:hover{
		background: #eee;
	}

	.font-montserrat .rounded-list a:hover:before{
		transform: rotate(360deg);
	}

	.font-montserrat .rounded-list a:before{
		content: counter(li);
		counter-increment: li;
		position: absolute;
		left: -20.8px;
		top: 50%;
		margin-top: -20.8px;
		background: #9fbd38;
		color: #fff!important;
		height: 43px;
		width: 43px;
		line-height: 32px;
		border: 4.8px solid #fff;
		text-align: center;
		font-weight: bold;
		border-radius: 32px;
		transition: all .3s ease-out;
	}
	.font-montserrat blockquote {
    border-left: 5px solid #fff;
    font-size: 14.5px;
	}
	.before-title{
		width: 90px;
	    background-color: #229296;
	    display: inline-block;
	    padding: 5px 39px 5px;
	    border-top-right-radius: 30px;
	    border-bottom-right-radius: 30px;
	    margin-left: -15px;
	}
	.font-montserrat .before-title .icon-before {
		border-radius: 50%;
	    width: 45px;
	    height: 45px;
	    color: #9fbd38;
	    text-align: center;
	    margin-right: 10px;
	    display: inline-block;
	    border: 2px solid #9fbd38;
	    background-color: #fff;
	    transition: all .3s ease-out;
	}
	.font-montserrat .before-title .icon-before:hover {
		transform: rotate(360deg);
	}
	.font-montserrat .step-label-conten{
		display: inline-block;
		font-size: 20px;
	}
	.font-montserrat .step-label-conten .step-label{
		border: 2px solid #9fbd38;
	    padding: 3px 5px;
	    border-radius: 5px;
	    background-color: #fff;
	    display: inline-block;
	    margin: 5px 1px 5px 1px;
	}
/*	#central-container #modalCoop{
    	min-height: 2460px;
	}*/
	.font-montserrat img.img-cospace{
		text-align: center;
	    margin: auto;
	    display: block;
	    max-width: 100%;
	    height: auto;
	}
	#central-container #modalCoop .modal-dialog .modal-content{
		width: 100%;
	    float: left;
	    box-shadow: none;
	}
	#central-container #modalCoop .modal-dialog .modal-content .modal-body{
		padding:0px; 
	}
	@media (max-width: 767px) {
		#central-container #modalCoop{
		 	/*min-height: 4000px;*/
		 	margin: 0px -15px;
	 	}
	 	#central-container #modalCoop .modal-dialog{
	 		margin: 0px;
	 	}
		#central-container .font-montserrat .letter-turq{
			text-align: center;
		}
	}
	@media (min-width: 768px) and (max-width: 991px){
		#central-container #modalCoop {
	    /*min-height: 2700px;*/
	    padding: 0px !important;
		}
	} 
</style>
<div class="font-montserrat margin-bottom-50">
	<h2 class="letter-turq">
	<span class="before-title hidden-xs">
		<span class="icon-before">
				<i class="fa fa-hand-o-up text-center" style="font-size: 24px;line-height: 40px;"></i>
			</span>
	</span>
        <?php echo Yii::t("cooperation","Welcome in your cooperative space")?></h2>
	<hr>

	<div class="well">
		<h3 class="label-question"><?php echo Yii::t("cooperation","What is it useful ?")?></h3>
		<p class="paragraph">
            <?php echo Yii::t("cooperation","The cooperative space can be seen as a collaborative project management tool, allowing for a form of <b>transparent and horizontal governance</b>.")?>
			<br>
			<br>

            <?php echo Yii::t("cooperation","It is a decision-making tool, which will allow you to make <b>collective decisions</b>, in consultation with all the")?>
			<?php if($type == Organization::COLLECTION){ ?> <?php echo Yii::t("cooperation","members of your organization.")?><?php } ?>
			<?php if($type == Project::COLLECTION){ ?> <?php echo Yii::t("cooperation","contributors to your project.")?><?php } ?>
			<br>
            <?php echo Yii::t("cooperation","Il vous permettra également de gérer les différentes <b>actions</b> (tâches) à réaliser dans le cadre de votre activité,et d'attribuer ces actions à vos")?>
			<?php if($type == Organization::COLLECTION){ ?> <?php echo Yii::t("cooperation","membres")?>.<?php } ?>
			<?php if($type == Project::COLLECTION){ ?> <?php echo Yii::t("cooperation","contributors")?>.<?php } ?>
		</p>
	</div>
	


	<div class="well">
		<h3 class="label-question"><?php echo Yii::t("cooperation","How does it work ?")?></h3>
		<p class="paragraph">
            <?php echo Yii::t("cooperation","Because every")?>
			<?php if($type == Organization::COLLECTION){ ?> <?php echo Yii::t("cooperation","organization is different")?>, <?php } ?>
			<?php if($type == Project::COLLECTION){ ?> <?php echo Yii::t("cooperation","project is different")?>, <?php } ?>
            <?php echo Yii::t("cooperation","you will start by creating thematic spaces according to your needs.<br>For example, if you manage a sports club, you can create a space called \"Use of the club's budget\", in which your members can make their proposals in relation to this theme.<br><br>Each space created in this way can receive proposals and actions related to the theme of the space.")?>
			<br><br>
		</p>
	</div>

	<hr>

	<div class="text-white" style="margin-bottom: 15px">
			<span class="label-start">&nbsp; <?php echo Yii::t("cooperation","Let's go!")?></span>
			<span class="icon-before-label">
				<i class="fa fa-rocket text-center" style="font-size: 25px;line-height: 47px;"></i>
			</span> 
	</div>

	<p>
        <?php echo Yii::t("cooperation", "Create your first room clicking on the following button") ?>
		<a href="javascript:dyFObj.openForm('room')" class="letter-green bold">
	  		<i class="fa fa-plus-circle"></i> <?php echo Yii::t("cooperation", "Create room") ?>
	  	</a><br>
	  	<?php echo Yii::t("cooperation", "You will find this button again in the left menu of your screen") ?>
	  	<br><br>
	  	<?php echo Yii::t("cooperation", "Your new room will appear on the left menu too, and you will be able to add your first proposal clicking on this button"); ?> <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/dda_help/addproposal.png" height=30> 
	</p>

	<div class="step-label-conten"> 
		<span class="step-label"><?php echo Yii::t("cooperation","Suggest")?></span>
		<span><i class="fa fa-angle-double-right text-green"></i></span> 
		<span class="step-label"><?php echo Yii::t("cooperation","amend")?></span>
		<span><i class="fa fa-angle-double-right text-green"></i></span> 
		<span class="step-label"><?php echo Yii::t("cooperation","vote")?></span>
		<span class="text-green"> ...</span>
	</div>
	<h5 style="padding-top: 10px;">
		<span><i class="fa fa-hand-o-right text-green"></i></span>
        <?php echo Yii::t("cooperation","These are the 3 essential steps of the collective decision process that we propose to you:")?>
	</h5>
		<ul class="rounded-list padding-15 ">
			<li>
				<a href="javascript:;"> <?php echo Yii::t("cooperation","Suggest")?> : </a>
				<blockquote>
                    <?php echo Yii::t("cooperation","A proposal is a text written by a")?>
				<?php if($type == Organization::COLLECTION){ ?> <?php echo Yii::t("cooperation","member of your organization")?><?php } ?>
				<?php if($type == Project::COLLECTION){ ?> <?php echo Yii::t("cooperation","contributor to your project")?><?php } ?>.
				<br>
                <?php echo Yii::t("cooperation","The author of a proposal can activate or deactivate the <i>amendment procedure</i>, depending on the need for it or not. He also defines its duration.<br>The author also defines the duration of the <i>voting procedure</i>, longer or shorter depending on the need for collective reflection around the proposed topic.")?>
				</blockquote>
			</li>
			<li>
				<a href="javascript:;">  <?php echo Yii::t("cooperation","amend")?> :</a>
				<blockquote>
                    <?php echo Yii::t("cooperation","An amendment is a modification, submitted to a vote, whose purpose is to correct, complete or cancel all or part of the proposal under deliberation.<br><i>(currently, it is only possible to complete the proposal by adding information. The modification, and deletion, are not yet available)</i><br><br>All")?>

					<?php if($type == Organization::COLLECTION){ ?> <?php echo Yii::t("cooperation","vote")?> <?php echo Yii::t("cooperation","member of your organization")?><?php } ?>
					<?php if($type == Project::COLLECTION){ ?> <?php echo Yii::t("cooperation","vote")?> <?php echo Yii::t("cooperation","contributor to your project")?><?php } ?>
                    <?php echo Yii::t("cooperation","can propose amendments to proposals.<br>Each amendment is voted on.<br>When the amendment period is over, the voted amendments are automatically added to the original proposal, and the voting period begins.<br><br><i>Reminder: the amendment period can be deactivated by the author of the proposal, in order to directly launch the voting process.</i>")?>
				</blockquote>
			</li>
			<li>				
				<a href="javascript:;">  <?php echo Yii::t("cooperation","vote")?> :</a>
				<blockquote>
                    <?php echo Yii::t("cooperation","When the amendment period is over (or disabled), the voting period begins.<br> Each")?>
					<?php if($type == Organization::COLLECTION){ ?> <?php echo Yii::t("cooperation","member")?><?php } ?>
					<?php if($type == Project::COLLECTION){ ?> <?php echo Yii::t("cooperation","contributor")?><?php } ?>
                    <?php echo Yii::t("cooperation","can then give their opinion by voting")?>
				</blockquote>
				
			
				<img class="img-responsive img-cospace" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/dda_help/vote.png"> 				
			</li>
		</ul>


	
	<div class="text-white" style="margin-bottom: 15px">
			<span class="label-start"><?php echo Yii::t("cooperation","And then")?> ?</span>
			<span class="icon-before-label" style="margin-left: -215px;">
				<i class="fa fa-angle-double-right  text-center" style="font-size: 25px;line-height: 47px;"></i>
			</span> 
	</div>

	

	<p style="font-size: 13px;">
		<?php echo Yii::t("cooperation", "When voting time is over, the proposal is closed, and becomes a <i>resolution</i><br/>You can find out all resolutions <b class='letter-green'>accepted</b> or <b class='letter-red'>refused</b> in each room in the following section :"); ?>
		<br><br>
		<img class="img-cospace" src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/dda_help/resolution.png">
	</p>

	

	<div class="well">
		<h3 class="label-question"><?php echo Yii::t("cooperation","What about stocks?")?></h3>
		<p class="paragraph">
            <?php echo Yii::t("cooperation","Actions can be created freely in each space, according to your needs.<br>They can also be created directly following each adopted proposal, to implement the decisions.")?>
		</p>
	</div>

</div>