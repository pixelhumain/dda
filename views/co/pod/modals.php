<style type="text/css">
  #menu-room h4 small{
    line-height: inherit!important;
  }
  #coop-container h4 {
    letter-spacing: inherit!important;
    line-height: 1.42857143;
  }
  #menu-room h4.room-desc{
    width: 100%!important;
  }
  @media (max-width: 767px) {
    .modal-dialog {
      margin: 60px 10px 10px 10px;
    }
    #coop-container h3.letter-turq{
      margin-top: 50px!important;
    }
  }
</style>
<?php $iconColor=(isset($element["type"]) && Element::getColorIcon($element["type"]) !== false) ? Element::getColorIcon($element["type"]) : Element::getColorIcon($element["collection"]);
?>

<!-- ************ MAIN MODAL CO-SPACE ********************** -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalCoop" style="z-index: 100000">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close margin-5 padding-10" data-dismiss="modal" id="btn-close-coop"
        		aria-label="Close"><i class="fa fa-times"></i>
        </button>


        <button href="javascript:" class="btn btn-default btn-sm text-dark pull-right tooltips"
				id="btn-update-coop" onclick="uiCoop.startUI();" style="margin: 10px 10px 0 0;" data-original-title="<?php echo Yii::t("cooperation", "Reload window") ?>" data-placement="left">
	  			<i class="fa fa-refresh"></i> <?php echo Yii::t("cooperation", "Refresh data") ?>
	  	</button> 	
        	
        <div class="modal-title" id="modalText">
        	<img class="pull-left margin-right-15" src="<?php echo (isset($element["profilThumbImageUrl"])) ? $element["profilThumbImageUrl"]: $element["defaultImg"]; ?>" height=52 width=52 style="">
			<!-- <h4 class="pull-left margin-top-15"><i class="fa fa-connectdevelop"></i> Espace coopératif</h4> -->
        	 <div class="pastille-type-element bg-<?php echo $iconColor; ?> pull-left" style="margin-top:14px;"></div>
			     <h4 class="pull-left margin-top-15">
        	  <?php echo @$element["name"]; ?>
        	</h4>        	
        </div>
      </div>
      
       <div class="modal-body col-lg-12 col-md-12 col-sm-12 col-xs-12">
			  <ul id="menuCoop" class="menuCoop col-lg-3 col-md-3 col-sm-3 col-xs-12">
    		</ul>
    		<div id="main-coop-container" class="col-lg-9 col-md-9 col-sm-9 col-xs-12"></div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- ************ MAIN MODAL CO-SPACE ********************** -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalTools" style="z-index: 100001">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close margin-5 padding-10" data-dismiss="modal" id="btn-close-coop"
            aria-label="Close"><i class="fa fa-times"></i>
        </button>

        <?php 
            if( isset( Yii::app()->session['userId']) ){
              $me = Element::getByTypeAndId("citoyens", Yii::app()->session['userId']);
              $profilThumbImageUrl = Element::getImgProfil($me, "profilThumbImageUrl", $this->module->assetsUrl);
              //$countNotifElement = ActivityStream::countUnseenNotifications(Yii::app()->session["userId"], Person::COLLECTION, Yii::app()->session["userId"]);
        ?> 
         <!-- #page.type.citoyens.id.<?php echo Yii::app()->session['userId']; ?> -->
        <a  href="#page.type.citoyens.id.<?php echo Yii::app()->session['userId']; ?>"
            class="menu-name-profil lbh text-dark pull-right shadow2" 
            data-toggle="dropdown">
                <small class="hidden-xs hidden-sm margin-left-10 bold" id="menu-name-profil">
                    <?php echo @$me["name"] ? $me["name"] : @$me["username"]; ?>
                </small> 
                <img class="img-circle" id="menu-thumb-profil" 
                     width="40" height="40" src="<?php echo $profilThumbImageUrl; ?>" alt="image" >
        </a>
        <?php } ?>
  
          
        <div class="modal-title" id="modalText">
          <img class="pull-left margin-right-15" src="<?php echo $thumbAuthor; ?>" height=52 width=52 style="">
      <!-- <h4 class="pull-left margin-top-15"><i class="fa fa-connectdevelop"></i> Espace coopératif</h4> -->
           <div class="pastille-type-element bg-<?php echo $iconColor; ?> pull-left" style="margin-top:14px;"></div>
           <h4 class="pull-left margin-top-15">
            <?php echo @$element["name"]; ?>
          </h4>         
        </div>
      </div>
      
       <div class="modal-body col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <ul id="menuCoop" class="menuCoop col-lg-2 col-md-3 col-sm-3  col-xs-12">
        </ul>
        <div id="main-coop-container" class="col-lg-10 col-md-9 col-sm-9  col-xs-12"></div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- ************ MODAL HELP COOP ********************** -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalHelpCOOP" style="z-index: 100001">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

       <div class="modal-body padding-25">
		  <?php 
      $tpl = "dda.views.co.pod.home";
      if(@$this->costum["modules"]["dda"]["tpls"]["home"])
        $tpl = $this->costum["modules"]["dda"]["tpls"]["home"];
      
      echo $this->renderPartial($tpl, array("type"=>$type)); 
      ?>
      </div>

      <div class="modal-footer">
      	<div id="modalAction" style="display:inline"></div>
        <button class="btn btn-default pull-right btn-sm margin-top-10 margin-right-10" data-dismiss="modal"> J'ai compris</button>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- ************ MODAL DELETE ROOM ********************** -->
<div class="modal fade" tabindex="-1" role="dialog" id="modalDeleteRoom" style="z-index: 100001">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        	<span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-title" id="modalText">
        	<h4><i class="fa fa-times"></i> <?php echo Yii::t("cooperation","Delete a room") ?></h4>
        </div>
      </div>
      <div class="modal-body">
      	<h3 style="text-transform: none!important; font-weight: 200;" class="letter-turq">
      		<i class="fa fa-hashtag"></i> <span id="space-name"><?php echo @$room["name"]; ?></span>
      	</h3>
      	<label><?php echo Yii::t("cooperation", "Are you sure to delete this cooperative space ?") ?></label><br>
      	<small class="text-red"><?php echo Yii::t("cooperation", "All the proposals, resolutions and actions linked to this room will be deleted") ?>.</small>
      </div>
      <div class="modal-footer">
      	<div id="modalAction" style="display:inline"></div>
        <button class="btn btn-danger pull-right btn-sm margin-top-10" 
				id="btn-delete-room" data-placement="bottom" 
				data-dismiss="modal"
				data-original-title="<?php echo Yii::t("cooperation","Delete the room : {what}", array("{what}"=>isset($room["name"]))); ?>"
				data-id-room="">
			<i class="fa fa-trash"></i> <?php echo Yii::t("cooperation", "Yes, delete this room") ?>
		</button>
		<button class="btn btn-default pull-right btn-sm margin-top-10 margin-right-10" data-dismiss="modal"> <?php echo Yii::t("common","Cancel") ?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
