<li class="submenucoop focus sub-resolutions no-padding col-lg-4 col-md-6 col-sm-6"
	data-name-search="<?php echo str_replace('"', '', isset($resolution["name"])); ?>">
	<a href="javascript:;" class="load-coop-data" data-type="resolution" 
      data-menu="true"
	   data-status="<?php echo @$resolution["status"]; ?>" 
	   data-dataid="<?php echo (string)@$resolution["_id"]; ?>">

  		<span class="elipsis">
  			<i class="fa fa-hashtag"></i> 
  			<?php if(isset($resolution["name"])) 
  					   echo $resolution["name"]; 
  				  else echo "<small><b>".
  				  		substr(isset($resolution["description"]), 0, 150).
  				  		   "</b></small>";
  			?>
  		</span>
  	</a>
</li>