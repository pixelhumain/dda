<?php 
	HtmlHelper::registerCssAndScriptsFiles( 
		array(  '/js/comments.js') , 
		Yii::app()->theme->baseUrl. '/assets');
 	

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.6.0/Chart.min.js",
		"/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css",
		'/plugins/facemotion/faceMocion.css',
   		'/plugins/facemotion/faceMocion.js',
		'/plugins/underscore-master/underscore.js',
		'/plugins/showdown/showdown.min.js',
		'/plugins/jquery-mentions-input-master/jquery.mentionsInput.js',
		'/plugins/jquery-mentions-input-master/jquery.mentionsInput.css',
		'/plugins/jquery-mentions-input-master/lib/jquery.events.input.js',
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

 	//var_dump($data); exit;
 	//var_dump($data);exit;
 	$element = Element::getByTypeAndId($data[$type]["parentType"], $data[$type]["parentId"]);
 	$iconColor = Element::getColorIcon($data[$type]["parentType"]);
  	$imgDefault = $this->module->assetsUrl.'/images/thumbnail-default.jpg';
	$thumbAuthor =  @$element['profilThumbImageUrl'] ? 
                      Yii::app()->createUrl('/'.@$element['profilThumbImageUrl']) 
                      : Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl().'/images/thumbnail-default.jpg';

    $slugParent = @$element["slug"] ? "@".$element["slug"] : "#page.type.".$data[$type]["parentType"].".id.".$element["_id"];

    if(@$data[$type]["idParentRoom"])
    $urlInCoSpace = "#".$slugParent.".view.coop.room.".$data[$type]["idParentRoom"].".".$type.".".$dataId;
?>

<style>
	#modal-preview-coop #coop-container{
		overflow-y: scroll;
		/*top:70px;*/
	}
	#modal-preview-coop #coop-data-container{
		border:0px;
	}
	#modal-preview-coop .btn-extend-proposal,
	#modal-preview-coop .btn-minimize-proposal,
	#modal-preview-coop .btn-extend-action,
	#modal-preview-coop .btn-minimize-action,
	#modal-preview-coop .btn-extend-resolution,
	#modal-preview-coop .btn-minimize-resolution{
		display: none;
	}
	#modalAssignMe,
	#ajax-modal{
		z-index:20000;
	}
</style>



<div id="coop-container" class="col-xs-12">
	<div id="coop-data-container" class="col-xs-12 no-padding">
		<div id="main-coop-container" class="col-xs-12 no-padding">
			<?php echo $this->renderPartial($type, $data); ?>
		</div>
	</div>
</div>