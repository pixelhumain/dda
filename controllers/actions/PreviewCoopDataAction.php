<?php

namespace PixelHumain\PixelHumain\modules\dda\controllers\actions;

use CAction, Cooperation, Yii;
class PreviewCoopDataAction extends \PixelHumain\PixelHumain\components\Action {

	public function run() { 

		$type 	= @$_POST["type"];
		$dataId 	= @$_POST["dataId"];

		$controller=$this->getController();

		$data = Cooperation::getCoopData(null, null, $type, null, $dataId);

		return $controller->renderPartial("preview", array("data" => $data, 
														 "type"=> $type, 
													 	 "dataId" => $dataId), true);
		Yii::app()->end();
		

		
	}



}
