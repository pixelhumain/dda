<?php

namespace PixelHumain\PixelHumain\modules\dda\controllers;

use Authorisation;
use CommunecterController;
use Yii;

/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CoController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		Yii::$app->response->headers->set('Access-Control-Allow-Origin', '*');
		Yii::$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
		Yii::$app->response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');

		// si la requête est de type OPTIONS, on retourne une réponse 200
		if($_SERVER[ 'REQUEST_METHOD' ] === 'OPTIONS'){
			Yii::$app->response->setStatusCode(200);
			return Yii::$app->response->send();
		}
		$headers = getallheaders();
		if( isset($headers["X-Auth-Token"]) && isset($headers["X-User-Id"]) && isset($headers["X-Auth-Name"]) && Authorisation::isMeteorConnected( $headers["X-Auth-Token"], $headers["X-User-Id"], $headers["X-Auth-Name"] ) ){
			$prepareData = false;
		} else if (isset($headers['Authorization']) && preg_match('/Bearer\s(\S+)/', $headers['Authorization'], $matches)) {
			Authorisation::isJwtconnected($matches[1]);
		}
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
	        'index'  => \PixelHumain\PixelHumain\modules\dda\controllers\actions\IndexAction::class,
	        'getcoopdata' => \PixelHumain\PixelHumain\modules\dda\controllers\actions\GetCoopDataAction::class,
	        'savevote' => \PixelHumain\PixelHumain\modules\dda\controllers\actions\SaveVoteAction::class,
	        'deleteamendement' => \PixelHumain\PixelHumain\modules\dda\controllers\actions\DeleteAmendementAction::class,
	        'getmydashboardcoop' => \PixelHumain\PixelHumain\modules\dda\controllers\actions\GetMyDashboardCoopAction::class,
	        'previewcoopdata' => \PixelHumain\PixelHumain\modules\dda\controllers\actions\PreviewCoopDataAction::class,
	        'getproposals' => \PixelHumain\PixelHumain\modules\dda\controllers\actions\GetProposalsAction::class,
	    );
	}

}

//co2/cooperation/getcoopdata > dda/co/getcoopdata